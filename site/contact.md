---
title: Contact
x-toc-enable: true
...

IRC channel `#untitled` on `irc.libera.chat`. Guides for connecting, via
popular IRC clients:

* <https://libera.chat/guides/hexchat>
* <https://libera.chat/guides/weechat>
* <https://libera.chat/guides/irssi>

(IRC is oldschool chatrooms, the way it's meant to be)

The title is a lie.

AND/OR:

Leah Rowe, at email address: [leah@libreboot.org](mailto:leah@libreboot.org)
