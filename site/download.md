---
title: Downloads
...

Install Git in your operating system, and download Untitled from this Git
repository:

<https://codeberg.org/vimuser/untitled>

There are no formal releases yet, but the Git repository should suffice. You
do not need to compile anything, because Untitled is written in posix sh, uses
posix commands for the most part, and mostly only requires Pandoc to be
installed.

You can also download *this website* from the following repository:

<https://codeberg.org/vimuser/untitled-website>

Check the home page for instructions on how to use Untitled.

Submit patches
==============

If you wish to send patches for improving either the software or the website,
there are instructions on the [code review page](git.md)
