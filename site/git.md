---
title: Code review
x-toc-enable: true
...

Untitled has a Git repository hosted here:
<https://codeberg.org/vimuser/untitled>

The *website* (including documentation) is hosted here (yes, it's an Untitled
site!):
<https://codeberg.org/vimuser/untitled-website>

Development of both software and website are done using the *Git* version
control system.
You can freely send patches to *either* repository, if you wish. If you do not
know how to use Git, see: <https://git-scm.com/>

Make an account on codeberg.org, and send a patch there via *Pull Request*.
The way pull requests work there is very similar interface-wise to what you
would see on another popular git hosting site, which we all know about, and will
not be named here due to it's proprietary nature.

Notabug runs on Gogs, which is Free Software.

Alternatively, you can simply send your patch to Leah Rowe directly:
[leah@libreboot.org](mailto:leah@libreboot.org)

Patches are very much welcome! Look at the [tasks page](/tasks/) to see ways
in which you can contribute code/documentation to the project.

You can also submit bug reports on the codeberg page, linked above.
