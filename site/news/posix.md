% Reliability fixes, and POSIX compatibility
% Leah Rowe
% 4 December 2023

Untitled is shaping up nicely. Recently, Untitled has undergone massive
re-factoring, greatly improving the code efficiency. Error handling has been
greatly improved, and the code is generally far less error-prone.

Many behaviours have improved. For example, Untitled will no longer delete HTML
files when cleaning the site (either partially or fully); instead, the target
HTML files will be overwritten in-place, when running `./build` and a `./roll`
option is available, which does a full re-build without deleting HTML files, but
otherwise has the same behaviour as `./build`.

The `-i` option in sed is no longer used, and other commands used in the
Untitled script have been audited heavily. Untitled has been converted to
POSIX `sh` scripts, instead of BASH scripts. This means that Untitled will now
work on many other operating systems easily (e.g. OpenBSD).

Several other minor tweaks have been made. For example, detection of site-wide
changes is a bit more reliable. The sitemap is now *always* generated, by
default, but an override option for `site.cfg` has been added, to allow turning
it off per-site. The *news indexes* are always re-generated now, whereas they
previously did not get updated correctly in some circumstances.

TL;DR Untitled is more robust now.

This article pertains to Untitled
revision `9cc6f5dc7349b7f339f99c84789b6c62ea7bb1c7`, on 4 December 2023.
