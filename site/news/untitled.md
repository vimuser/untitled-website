% Untitled
% Leah Rowe
% 20 May 2021 (updated 22 April 2023)

**UPDATE ON 22 APRIL 2023: [Git hosting moved to Codeberg](codeberg.md)**

Untitled static site generator was released 2 days ago via Notabug:
<https://notabug.org/untitled/untitled>

However, at that time, the website did not exist. That website now does exist.
You are reading it now!

Happy hacking?

Stay tuned for future project updates!
