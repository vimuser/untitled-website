% Untitled git now hosted on codeberg
% Leah Rowe
% 22 April 2023

Untitled was originally hosted here:
<https://notabug.org/untitled/untitled>

It has now moved here:

* <https://codeberg.org/vimuser/untitled>
* <https://codeberg.org/vimuser/untitled-website>

I'll probably still push to Notabug. Any issues/PRs there will be dealt
with at some point.

Notabug is old gogs, whereas codeberg uses newer forgejo based upon gitea,
itself a fork of gogs. Nicer features are available, such as searchable issues.

The title is a lie.
