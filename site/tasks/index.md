---
title: Tasks
x-toc-enable: true
...

improved language support
=========================

Right now, the way multi-language pages work is very hacky, and English-as-default
is hardcoded into the logic.

Make the site i18n-compliant. For instance, GNU Gettext could be used (or the
site owner could just maintain whole pages that are translated).

Have directories in untitled, for each language. it will have messages for
printf's used, such as links, specify page language, page direction (left to
right or right to left), what character encoding to use, etc.

daemonize everything
====================

Could also write a daemon program to detect changes in real time, and
update specific pages. Functions would also be needed to selectively update
the news and sitemap pages on each site.

It already is possible to do this, by simply running `./build` in a loop, but
this is very heavy on disk I/O so would cause excessive wear/tear. It also
doesn't scale well on very large websites (think: millions of pages).

Fix cursed RSS code
===================

The code is actually not bad, but could be better.

Fix cursed title code
=====================

Code for finding title, author and date of pages is very fragile, and BARELY
works, and makes precise assumptions about the type of Markdown variant is in
use, even though Pandoc supports many variants.

Don't sed s/md/html on docs
===========================

`md` links are converted to `html` links, which is correct, but pages that
make reference to `md` files while including the `.` character will be mangled
by this.

The sed replacement is not performed on external links, which is correct, but
incomplete.

support tagging
===============

For instance, a page about cats could have the tags:

* cats
* kittens
* felines

Then you would have a directory called `tag/` automatically generated on the
site, with a directory for each tag, e.g.:

* tag/cats/index.md
* tag/kittens/index.md
* tag/felines/index.md

Make this feature optional, because it would be quite heavy on disk I/O.

petition pages
==============

Such pages could actually be used for many other purposes. An example of a
petition page is here:

<https://rms-support-letter.github.io/>

This could be very useful in general. For example, the [contrib](../contrib.md)
page on this very website could use such a feature!

auto-sort news pages
====================

Right now, a MANIFEST file needs to be present in a news directory. However,
a condition where only `news.cfg` exists could be supported, and pages sorted
automatically based on file *creation* dates (not modification dates).

This would only be a fallback feature, to make the News feature easier to use.
The best option is still to use a MANIFEST file.

unorganised todo
================

copied from the untitled src prior to cleanup:

```
# TODO: speed optimization: use ***GIT*** to detect site changes.
#		init .git in www/ but already have a .gitignore (part of
#		untitled) in there, which only allows .md files. that way,
#		git add -A . will always, always only add .md files. then
#		git-whatchanged can be used, always, to just detect changes
#	one thing:
#		it is not possible, using this method, to detect all changes.
#		for example, .include files (and their per-page equivalents)
#		so on some files, modification dates will still be used

# TODO: support MANIFEST.xx for translated news indexes. Individual news pages
#		are still supported, without this change

# TODO: make site change detection more robust. certain things are missed
#		AUDIT NEEDED

# TODO: make untitled work on subdirectories. e.g. https://website.com/~user/

# TODO: gemtext support

# TODO: write an optimized C replacement for pandoc markdown->html conversion
#		pandoc is written is haskell, and C will probably be faster

# TODO: ensure that /tmp is a tmpfs, and recommend this in the documentation

# TODO: in mkhtml, allow auto-nav if no .nav file specified
#		but disable this by default, to maintain old behaviour
#		make it configurable in site.cfg

# TODO: next/previous links on news pages

# TODO: allow per-directory .include overrides, including for translations
#		right now, only global ones and per-page ones are supported
```
