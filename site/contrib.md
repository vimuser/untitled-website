---
title: Authors
x-toc-enable: true
...

Introduction
============

This page attempts to credit those who make Untitled possible. If you're not
listed here, please say so and your name will be added!

Leah Rowe
=========

Leah is the creator and lead developer of the Untitled multi-site static site
generator. Leah is also the founder and lead developer of the following free
software projects:

* <https://libreboot.org/>
* <https://osboot.org/>

Leah's personal website is:
<https://vimuser.org/> and has a blog:
<https://blog.vimuser.org/>

Alyssa Rosenzweig
=================

Alyssa was not involved with the creation of Untitled itself, but she wrote the
original static site generator that was first used on <https://libreboot.org/>

*That* static site generator was forked many times, with different features
added, for various websites operated by Leah Rowe. Untitled was created to
combine all the features of those forks, so as to consolidate management of
each website under one roof: the Untitled multi-site static site generator!

The Libreboot website now uses Untitled.

Without Alyssa's initial contribution, Untitled would not exist. You can see
Alyssa's website here: <https://rosenzweig.io/>

Fun fact: Alyssa also used to be a member of the Libreboot project. You can read
her entry on the Libreboot contributors page:
<https://libreboot.org/contrib.html#alyssa-rosenzweig>

Alyssa is the leader and founder of the Panfrost project, which provides a
completely free set of driver code for Mali Txxx and Gxx GPUs. You can see that
project here:
<https://gitlab.freedesktop.org/panfrost> and the official home page is
here: <https://panfrost.freedesktop.org/>

Pandoc project
==============

Since Untitled relies heavily on Pandoc to generate static HTML pages, it can
be said that Untitled would not exist without Pandoc! The official Pandoc
website is here:

<https://pandoc.org/>
